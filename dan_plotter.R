library(plotly)
library(dplyr)
library(tidyr)


##### BUILD THE BAR PLOTS #####
# Produce a summary of the parcentage of people wrt their response to a question
compute_bar_data <- function(data, question, option=NULL, by=NULL){
  if (!is.null(option) && !is.null(by)){
    bar_data <- dataSelectSub(data, question, option, by)
  } else {
    bar_data <- dataSelect(data, question)
  }
  return(bar_data)
}

# Plot the data on a bar chart
plot_bars <- function(table, question){
  len <- 0
  for (i in 1:length(table$var)) {
    if (nchar(table$var[i]) > len) {len <- nchar(table$var[i])}
  }
  plt <- plot_ly(table) %>% 
    add_trace(x = ~perc, y = ~var, orientation = "h") %>% 
    layout(title = question,
           yaxis = list(title = ""),
           xaxis = list(title = "Percentage"),
           margin = list(l = len*7))
  return(plt)
}

# Function to encapsulte the plotting of the bar chart.
# data is the master data; section is the column whose length needs
# computing for the percentages, e.g. data$Segment
produce_bar_chart <- function(data, question, option = NULL, by = NULL){
  quest <- selectID(question)
  plt <- data %>% compute_bar_data(quest, option, by) %>% 
    plot_bars(question)
  return(plt)
}
##########


##### BUILD THE TICK QUESTION PLOTS #####
# Extract the percentages of respondants per subcategory vs all respondants.
# Output is filtered by the desired topic.
# *GT functions get information for tick questions
extract_percsGT <- function(input_data, topic, subcat, filt){
  init_data <- getQuestInfoGT(input_data) %>% 
    dplyr::filter(Topic == topic) %>% 
    dplyr::select(-Xscore, -question_type)
  next_data <- getQuestDataGT(init_data$id, subcat, filt) %>% 
    gather(id, Percentage, -subcat, -counts)
  percs <- left_join(init_data, next_data) %>% 
    dplyr::select(-description, Topic)
  stats_data <- getQuestDataCountsGT(init_data$id, subcat, filt)
  stats_data <- do_stats_test_tick_questions(stats_data, "fisher")
  percs <- left_join(percs, stats_data) %>% 
    tidyr::unite(SubQuestion, sig_flag, SubQuestion, sep=' ')
  return(percs)
}

# Plot the bar chart for the tick questions
plot_ticks <- function(a_table, topic, subcat){
  len <- 0
  for (i in 1:length(a_table$SubQuestion)) {
    if (nchar(a_table$SubQuestion[i]) > len) {len <- nchar(a_table$SubQuestion[i])}
  }
  x1 <- a_table %>% select(SubQuestion) %>% unique()
  y1 <- a_table %>% filter(a_table[[subcat]] == a_table[[subcat]][[1]])
  y2 <- a_table %>% filter(a_table[[subcat]] == a_table[[subcat]][[2]])
  p <- plot_ly(a_table) %>% 
    add_trace(x = ~x1$SubQuestion, y = ~y1$Percentage, type = "bar", name = y1[[subcat]][[1]]) %>% 
    add_trace(x = ~x1$SubQuestion, y = ~y2$Percentage, type = "bar", name = y2[[subcat]][[1]]) %>% 
    layout(xaxis = list(title = ""),
           yaxis = list(title = "Percentage"),
           title = topic,
           margin = list(b = len*3))
  return(p)
}

# Function to encapsulate the functions that produce the bar charts of the 
# tick questions.
produce_tick_bars <- function(input_data, topic, subcat, filt){
  plt <- input_data %>% extract_percsGT(topic, subcat, filt) %>% 
    plot_ticks(topic, subcat)
  return(plt)
}
##########


##### BUILD THE SCORE QUESTION PLOTS #####
# Extract the mean scores of respondants per subcategory vs all respondants.
# Output is filtered by the desired topic.
# *GS functions get information on score questions
extract_percsGS <- function(input_data, topic, subcat, filt){
  init_data <- getQuestInfoGS(input_data) %>% 
    dplyr::filter(Topic == topic) %>% 
    dplyr::select(-Xscore, -question_type)
  next_data <- getQuestDataGS(init_data$id, subcat, filt) %>% 
    gather(id, Percentage, -subcat, -counts)
  percs <- left_join(init_data, next_data) %>% 
    dplyr::select(-description, Topic)
  stats_data <- getQuestDataScoresGS(init_data$id, subcat, filt)
  stats_data <- do_stats_test_score_questions(stats_data, "t.test")
  percs <- left_join(percs, stats_data) %>% 
    tidyr::unite(SubQuestion, sig_flag, SubQuestion, sep=' ')
  return(percs)
}

#extract_percsGS(qstGS$Topic, "Drinking habits", "Segment", 1)

# Plot the BAR graph for the score questions
plot_lines <- function(a_table, topic, subcat){
  len <- 0
  for (i in 1:length(a_table$SubQuestion)) {
    if (nchar(a_table$SubQuestion[i]) > len) {len <- nchar(a_table$SubQuestion[i])}
  }
  x1 <- a_table %>% select(SubQuestion) %>% unique()
  y1 <- a_table %>% filter(a_table[[subcat]] == a_table[[subcat]][[1]])
  y2 <- a_table %>% filter(a_table[[subcat]] == a_table[[subcat]][[2]])
  p <- plot_ly(a_table) %>% 
    add_trace(x = ~x1$SubQuestion, y = ~y1$Percentage, type = "bar", name = y1[[subcat]][[1]]) %>% 
    add_trace(x = ~x1$SubQuestion, y = ~y2$Percentage, type = "bar", name = y2[[subcat]][[1]]) %>% 
    layout(xaxis = list(title = ""),
           yaxis = list(title = "Mean Score"),
           title = topic,
           margin = list(b = len*3))
  return(p)
}

extract_percsGS(qstGS$Topic, "Drinking habits", "Segment", 1) %>%
  plot_lines("Drinking habits", "Segment")

# Function to encapsulate the functions that produce the line graphs of the 
# score questions.
produce_line_graph <- function(input_data, topic, subcat, filt){
  plt <- input_data %>% extract_percsGS(topic, subcat, filt) %>% 
    plot_lines(topic, subcat)
  return(plt)
}
##########


##### BUILD OVERVIEW PLOTS #####
# Retrieve the data for the overview
get_overview <- function(data, question, breakdown) {
  loc_Data <- data %>% dplyr::filter(Segment != 9) %>% 
    dataSelectOvr(selectID(question), breakdown = breakdown)
  grp_loc_Data <- loc_Data %>% dplyr::group_by(Segment_Colour) %>% 
    dplyr::summarise(count = sum(count)) %>% 
    dplyr::mutate(perc =100*count/sum(count))
  grp_loc_Data$var <- rep('Whole population',nrow(grp_loc_Data))
  sel_data <- dplyr::bind_rows(loc_Data, grp_loc_Data)
  return(sel_data)
}

# Plot the overview data
plot_overview <- function(a_table, by_count=FALSE) {
  len <- 0
  for (i in 1:length(a_table$var)) {
    if (nchar(a_table$var[i]) > len) {len <- nchar(a_table$var[i])}
  }
  if (by_count){
    plt <- plot_ly(a_table) %>% 
      add_trace(x = ~count, y = ~var, orientation = 'h', type = 'bar', color = ~Segment_Colour) %>% 
      layout(barmode = 'stack',
             yaxis = list(title = ""),
             xaxis = list(title = "Count"),
             title = "Overview",
             margin = list(l = len*6)) 
  } else {
    plt <- plot_ly(a_table) %>% 
      add_trace(x = ~perc, y = ~var, orientation = 'h', type = 'bar', color = ~Segment_Colour) %>% 
      layout(barmode = 'stack',
             margin = list(l = len*6),
             yaxis = list(title = ""),
             xaxis = list(title = "Percentage"),
             title = "Overview")
  }
  return(plt)
}

# Function that encapsulates the data retrieval and graph plotting for the
# overview pane
produce_overview <-function(data, question, breakdown, by_count=FALSE) {
  plt <- data %>% get_overview(question, breakdown) %>% 
    plot_overview(by_count)
  return(plt)
}

##########